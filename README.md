# IRI freestyle motocross dataset
Michael Villamizar

mvillami@iri.upc.edu

http://www.iri.upc.edu/people/mvillami/

institut de robòtica i informàtica CSIC-UPC

08028 Barcelona, Spain

## General Description
This dataset was created for testing detection approaches considering object
rotations in the image plane. In particular, this dataset contains motorbikes
under multiple orientations and with difficult imaging conditions such as
partial occlusions, scale variations, lighting and intra-class changes, etc.

## Training Data
For training, two sets of images are used. The first one has a collection of
positive samples (object images), whereas the second one includes background or
negative samples. These sets of images were extracted from the Caltech
motorbikes dataset [1]. For the positive set, however, a reduced number of
images (65 images) has been chosen according to the criterion of selecting the
most related samples to the target motorbike model. As negative images, the
Caltech background dataset is used [1]. They represent the non-object class by
means of 900 example images of background. For more information about training
details refer to [2].

## Test Data
This dataset contains two sets of test images, the first one -test1- includes
images with one or more motorbikes without rotations in the image plane, hence,
they are facing right. This set contains 69 images that includes 78 motorbike
instances. On the other hand, the second set of images -test2- has motorbikes
with planar rotations. More specifically, this set contains 100 images with 128
motorbikes instances. Both sets were acquired from images in internet and
contain challenging conditions for object detection. 

## File Format
For testing, one annotation file per test image is given. This file includes
some relevant image information and the ground truth by means of a bounding
box. 

More precisely, every annotation file is provided in *.mat and contains the
following information:

imgName    -> image name.

numObjects -> the number of objects in the image.

objects    -> an struct containing the category, bounding box -bbox-, an
              informative color, and the orientation of every object -angle-
	      in the image. 

In addition:

bbox  -> follows the bounding box format: [left, top, width, height].

angle -> the object orientation is given in degrees.

## Code
In order to show the ground truth over the test images, a matlab file -fun_bounding_box- is also 
provided. The execution of this file shows every test image and its correspoding bounding boxes 
according to the annotation file. The rest files are neccesary to execute the former file.

## References
[1] R. Fergus, P. Perona, and A. Zisserman. Object class recognition by unsupervised scale-invariant 
learning. In CVPR, 2003.

[2] M. Villamizar, F. Moreno-Noguer, J. Andrade-Cetto and A. Sanfeliu. Efficient Rotation Invariant 
Object Detection using Boosted Random Ferns. In CVPR, 2010.


