%% IRI freestyle motocross dataset : drawing image
%
% Michael Villamizar
% mvillami@iri.upc.edu
% http://www.iri.upc.edu/people/mvillami/
% institut de robòtica i informàtica CSIC-UPC
% 08028 Barcelona, Spain
%
function outVar = fun_image_drawing(iImg)
if (nargin~=1), error('incorrect input parameters'); end

% normalization to 1
iImg = double(iImg);
iImg = iImg - min(iImg(:));
iImg = iImg./max(iImg(:));

% create image
if (size(iImg,3)~=3)
    drwImg = cat(3,iImg,iImg);
    drwImg = cat(3,iImg,drwImg);
else
    drwImg = iImg;
end

% output
outVar = drwImg;
end

