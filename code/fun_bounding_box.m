%% IRI freestyle motocross dataset : bounding box
%
% Michael Villamizar
% mvillami@iri.upc.edu
% http://www.iri.upc.edu/people/mvillami/
% institut de robòtica i informàtica CSIC-UPC
% 08028 Barcelona, Spain
%
function fun_bounding_box()
clc,close all,clear all
fun_show_messages('IRI freestyle motocross dataset','title');

% parameters
imgPath = '../test_images/test1/images/';
annPath = '../test_images/test1/annotations/';

% load image files
imgFiles = dir([imgPath,'*.jpg']);
numFiles = size(imgFiles,1);
fun_show_messages(sprintf('num. images -> %d',numFiles),'process');

% file iteration
for iterFile = 1:numFiles
    
    % image name
    name = imgFiles(iterFile).name;
    
    % load annotation
    data = fun_data_load(annPath,[name,'.mat']);
    
    % image
    img = imread([imgPath,name]);
    
    % drawing image
    img = fun_image_drawing(img);
    
    % objects
    for iterObj = 1:data.numObjects
        
        % bounding box and color
        bbox  = data.objects(iterObj).bbox;
        color = data.objects(iterObj).color;
        angle = data.objects(iterObj).angle;
        
        % draw rectangle
        img = fun_draw_rectangle(img,bbox,color,5,angle);
        
    end
    
    % show image
    figure,imshow(img)
   
    % message
    fun_show_messages(sprintf('image -> %s',name),'process');
    pause(2);
    close
    
end

% message
fun_show_messages('end','title');
end
