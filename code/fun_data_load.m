%% IRI freestyle motocross dataset : load data
%
% Michael Villamizar
% mvillami@iri.upc.edu
% http://www.iri.upc.edu/people/mvillami/
% institut de robòtica i informàtica CSIC-UPC
% 08028 Barcelona, Spain
%
function outVar = fun_data_load(iPath,iName)
if (nargin~=2), error('incorrect input parameters'); end

% load data
data = load([iPath,iName]);

% output
outVar = data.data;
end

