%% IRI freestyle motocross dataset : draw rectangle
%
% Michael Villamizar
% mvillami@iri.topc.edu
% http://www.iri.topc.edu/people/mvillami/
% institut de robòtica i informàtica CSIC-topC
% 08028 Barcelona, Spain
%
function outVar = fun_draw_rectangle(iImg,iBox,iColor,iThick,iAngle)
if (nargin~=5), error('incorrect input parameters'); end

% coordinates
left   = max(1,iBox(1));
top    = max(1,iBox(2));
width  = iBox(3);
height = iBox(4);
ang    = iAngle*pi/180;

% rotate rectangle
newRec = fun_rotate_rectangle(iBox,iAngle);

% center point
cx = round(left + width/2);
cy = round(top + height/2);

% rotation matrix
rotMat = [cos(ang),-sin(ang);sin(ang),cos(ang)];

% orientation line
tmp      = (top - cy):0;
lineMidY = [zeros(1,size(tmp,2));tmp];
lineMidY = round(rotMat*lineMidY);
lineMidY = lineMidY + [cx;cy]*ones(1,size(lineMidY,2)); 

% draw rotated lines
iImg = fun_draw_line(iImg,newRec.top,iThick,iColor);
iImg = fun_draw_line(iImg,newRec.bottom,iThick,iColor);
iImg = fun_draw_line(iImg,newRec.left,iThick,iColor);
iImg = fun_draw_line(iImg,newRec.right,iThick,iColor);
iImg = fun_draw_line(iImg,lineMidY,iThick,iColor);

% output
outVar = iImg;
end

%% rotate rectangle
function outVar = fun_rotate_rectangle(iRec,iAngle)

% rectangle coordinates
left   = iRec(1);
top    = iRec(2);
width  = iRec(3);
height = iRec(4);
right  = left + width;
bottom = top + height;
ang    = iAngle*pi/180;

% center point
cx = round(left + width/2);
cy = round(top + height/2);

% rotation matrix
rotMat = [cos(ang),-sin(ang);sin(ang),cos(ang)];

% line top
tmp     = left-cx:right-cx;
lineTop = [tmp;(top-cy)*ones(1,size(tmp,2))];
lineTop = round(rotMat*lineTop);
lineTop = lineTop + [cx;cy]*ones(1,size(lineTop,2)); 

% line bottom
tmp        = left-cx:right-cx;
lineBottom = [tmp;(bottom-cy)*ones(1,size(tmp,2))];
lineBottom = round(rotMat*lineBottom);
lineBottom = lineBottom + [cx;cy]*ones(1,size(lineBottom,2)); 

% line left
tmp      = top-cy:bottom-cy;
lineLeft = [(left-cx)*ones(1,size(tmp,2));tmp];
lineLeft = round(rotMat*lineLeft);
lineLeft = lineLeft + [cx;cy]*ones(1,size(lineLeft,2)); 

% line right
tmp       = (top-cy):(bottom-cy);
lineRight = [(right-cx)*ones(1,size(tmp,2));tmp];
lineRight = round(rotMat*lineRight);
lineRight = lineRight + [cx;cy]*ones(1,size(lineRight,2)); 

% new rotated rectangle : lines
newRec.top    = lineTop;
newRec.bottom = lineBottom;
newRec.left   = lineLeft;
newRec.right  = lineRight;

% output
outVar = newRec;
end

%% draw line
function outVar = fun_draw_line(iImg,iLine,iThick,iColor)

% line
for iter = 1:size(iLine,2)
    for iterThick = 0:iThick-1
        x = iLine(1,iter) + iterThick;
        y = iLine(2,iter) + 0;
        iImg(max(y,1),max(x,1),:) = iColor;
        x = iLine(1,iter) + 0;
        y = iLine(2,iter) + iterThick;
        iImg(max(y,1),max(x,1),:) = iColor;
    end
end

% output
outVar = iImg;
end

