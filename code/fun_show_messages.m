%% IRI freestyle motocross dataset : show messages
%
% Michael Villamizar
% mvillami@iri.upc.edu
% http://www.iri.upc.edu/people/mvillami/
% institut de robòtica i informàtica CSIC-UPC
% 08028 Barcelona, Spain
%
function fun_show_messages(iText,iMessage)
if (nargin~=2), error('incorrect input parameters'); end

% different types of messages
switch (iMessage)
    case 'title'
        tx = sprintf('******************** %s ********************',iText);
    case 'process'
        tx = sprintf('-> %s',iText);
    case 'error'
        tx = sprintf(':$ ERROR : %s ',iText);disp(tx);
        error('program error');
end

% print message
disp(tx)

end

