%% IRI freestyle motocross dataset : save data
%
% Michael Villamizar
% mvillami@iri.upc.edu
% http://www.iri.upc.edu/people/mvillami/
% institut de robòtica i informàtica CSIC-UPC
% 08028 Barcelona, Spain
%
function fun_data_save(iData,iPath,iName)
if (nargin~=3), error('incorrect input parameters'); end

% save
data = iData;
save([iPath,iName],'data');

end

